#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include <time.h>
#define MAX 30

typedef struct birthday{
	int month, day, year;
};

typedef struct DATA_LIST{
	char firstname[MAX];
	char lastname[MAX];
	int age;
	int idnumber;
	double phone_number;
	birthday bd;
	DATA_LIST *next;
};

int create_list();
void add_contact(DATA_LIST*);
void display_contact(DATA_LIST*);
void count_contacts(DATA_LIST *);
void delete_contact(DATA_LIST *, int);
int search_contact(DATA_LIST*, int);
DATA_LIST * Reverse(DATA_LIST *,char *, char *,int,int,double,int,int,int);

int main(){

				DATA_LIST *info = (DATA_LIST*)malloc(sizeof(DATA_LIST));
				info->next = NULL;
				int input =1;
				int NUMELS;

				while(1){
					 printf("\n\n*******  MENU  *******\n\n");
					 printf("1 - Create List\n");
					 printf("2 - Add Contact\n");
					 printf("3 - Search Contact\n");
					 printf("4 - Delete Contact\n");
					 printf("5 - Display Contacts\n");
					 printf("6 - Count Contacts\n");
					 printf("7 - Reverse List\n");
					 printf("0 - EXIT\nYour choice:   ");
					 scanf("%d",&input);
					 fflush(stdin);


					 switch(input){
									case 1:{
										NUMELS = create_list();
										printf(" ****************\n [ LIST CREATED ]\n ****************\n\n");
									break;
									}

									case 2:{
										for(int x=0;x<NUMELS;add_contact(info), fflush(stdin),x++);
										printf("*************************\n[ DONE ADDING CONTACTS! ]\n*************************\n\n");
									break;
									}

									case 3:{
										int search;

										printf("\nInput Directory I.D to be search: ");
										scanf("%d", &search);

										int result = search_contact(info, search);

										if(result == 0){
											printf(" *******************************\n [ \tSEARCHING FAILED!      ]\n [ NODE NOT FOUND IN DIRECTORY.]\n *******************************\n\n");
										}else{
											printf("***************************\n[ DATA FOUND AT INDEX: %d ]\n***************************\n", result);
										}
									break;
									}

									case 4:{
										int del;

										printf("\nInput Directory I.D to be deleted: ");
										scanf("%d", &del);

										delete_contact(info, del);

									break;
									}

									case 5:{
										 display_contact(info);
									break;
									}

									case 6:{
										count_contacts(info);
									break;
									}

									case 7:{
										DATA_LIST *start = info;
										char *fname[MAX];
										char *lname[MAX];
										int age[MAX];
										double cp[MAX];
										int d[MAX], m[MAX], y[MAX];

										int index = 0;

										while(info->next!=NULL){

											fname[index] = info->firstname;
											lname[index] = info->lastname;
											age[index] = info->age;
											cp[index] = info->phone_number;
											m[index] = info->bd.month;
											d[index] = info->bd.day;
											y[index] = info->bd.year;
											index++;
											info = info->next;
										}

											for(int x=0;x<MAX;x++){
												if(start->next == NULL){
													break;
												}else{
													info = Reverse(info,fname[x],lname[x],age[x],1+rand()%1000,cp[x],m[x],d[x],y[x]);
													start = start->next;
										}
									}
													printf("\n**********************\n[ REVERSE COMPLETED! ]\n**********************\n");
									break;
									}

									case 0:{
										printf("*********************\n[ EXIT COMPLETED!!! ]\n*********************");
										exit(1);
									break;
									}

									default:{
										printf("\n\n*********************\n[ INVALID SELECTION.]\n[ PLEASE TRY AGAIN! ]\n*********************\n");
									break;
									}
		}
	}

	getch();
	free(info);
	return 0;
}

int create_list(){
				int N;

				printf("Input numbers of data to be stored: ");
				scanf("%d", &N);
				fflush(stdin);

				return N;
}

void add_contact(DATA_LIST * info){

	FILE *fp = fopen("DATA.txt", "a");
	srand(time(NULL));
	int idno = 1+rand()%1000;

	while(info->next!=NULL){
		info = info->next;
	}

	info->next = (DATA_LIST*)malloc(sizeof(DATA_LIST));

	printf("\n\nFirst Name: ");
	gets(info->firstname);

	fflush(stdin);

	printf("Last Name: ");
	gets(info->lastname);

	fflush(stdin);

	printf("Age: ");
	scanf("%d",&info->age);

	printf("Phone No.:\n");
	scanf("%lf", &info->phone_number);

	fflush(stdin);

	printf("Birthdate:\n( mm/dd/yy ):\n  ");
	scanf("%d %d %d", &info->bd.month,&info->bd.day,&info->bd.year);

	printf("\n(REMEMBER!!!) YOUR DIRECTORY ID IS: %d\n", idno);

	printf("\n\n");
	fprintf(fp,"\r\nName: %s %s\r\nAge: %d\r\nDirectory I.D: %d\r\nPhone No.: 0%10.0lf\r\nBirthdate: %d/%d/%d\r\n************************", info->firstname, info->lastname,info->age, idno, info->phone_number,info->bd.month,info->bd.day,info->bd.year);
	fclose(fp);

	info->idnumber = idno;
	info->next->next = NULL;
}


void display_contact(DATA_LIST *info){

	printf("***********************\n[ DIRECTORY DATA LIST ]\n***********************\n");

	while(info->next!=NULL){
		printf("\n\nName: %s %s\nDirectory ID: %d\nAge: %d\nPhone No.: 0%10.0f\nBirthday: %d/%d/%d\n\n#######################",info->firstname, info->lastname,info->idnumber,info->age,info->phone_number, info->bd.month,info->bd.day,info->bd.year);
		info = info->next;
	}
}

void count_contacts(DATA_LIST *head){

	DATA_LIST *current;
	int count = 0;

	for(current=head;current!=NULL;current = current->next){
		count++;
	}

	printf("\n***********************************\n[ NUMBER OF NODES IN THE LIST: %d ]\n***********************************\n\n", count-1);
}

void delete_contact(DATA_LIST * theList, int num){

	while(theList->next!=NULL && theList->idnumber!=num){
		theList = theList->next;
	}

	if(theList->next == NULL){
		printf("******************* \n[ NODE NOT FOUND! ]\n*******************\n\n");
		return;
	}else{

	DATA_LIST * temp = theList->next;
	theList->next = temp->next;

	free(temp);
	printf(" *********************************************\n [ DATA SUCCESSFULLY DELETED FROM DIRECTORY! ]\n *********************************************\n\n");

	return;
	}
}


int search_contact(DATA_LIST * theList, int snumber){
	int fail = 0;
	int index = 1;

	while(theList->next!=NULL && theList->idnumber!=snumber){
		index++;
		theList = theList->next;
	}

	if(theList->next == NULL){
		return fail;
	}else{
		return index;
	}
}


DATA_LIST * Reverse(DATA_LIST *head,char *fn, char *ln,int age,int id, double cp, int m, int d, int y){

	DATA_LIST * newList = (DATA_LIST *)(malloc(sizeof(DATA_LIST)));

	strcpy(newList->firstname,fn);
	strcpy(newList->lastname,ln);
	newList->age = age;
	newList->idnumber = id;
	newList->phone_number = cp;
	newList->bd.month = m;
	newList->bd.day = d;
	newList->bd.year = y;

	newList->next = head;
	return newList;
}
