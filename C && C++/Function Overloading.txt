#include <stdio.h>
#include <conio.h>

void printNumber(int x);
void printNumber(float y);

    int main(){
        int a=1;
        float b=12.5597;

        printNumber(a);
        printNumber(b);
    }

void printNumber(int x){
    printf("I am printing an integer %d\n", x);
}

void printNumber(float y){
    printf("I am printing a float number %f\n",y);
}
