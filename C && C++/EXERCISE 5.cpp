#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

typedef struct NODE{
	char data;
	NODE * next;
};
/*
C A N
1 2 3
*/

void add(NODE *);
void print(NODE *);
void insert_first(NODE **,char);
void insert_second(NODE **, char);
void insert_third(NODE **, char);
void insert_last(NODE **, char);

	int main(){
		char a;
		int choice;

		NODE * list = (NODE*)malloc(sizeof(NODE));
		list->next = NULL;

		add(list);
		printf("CURRENT ELEMENTS:\n");
		print(list);

		printf("Enter a character to insert: ");
		scanf("%c", &a);

		printf("1 - Store character before C\n2 - Store character after C\n3 - Store character after A\n4 -  Store character after N\n");
		scanf("%d", &choice);
		switch(choice){
			case 1:
				insert_first(&list, a);
			break;

			case 2:
				insert_second(&list, a);
			break;

			case 3:
				insert_third(&list, a);
			break;

			case 4:
				insert_last(&list, a);
			break;

			default:
				printf("Invalid Selection!\n");
			break;
		}

		print(list);
		getch();
		return 0;
	}

void add(NODE * list){
	int i = 0;
	char constant[3] = {'C','A','N'};

	start:

	while(list->next!=NULL){
		list = list->next;
	}

	list->next = (NODE *)malloc(sizeof(NODE));
	list->data = constant[i];
	list->next->next = NULL;
	i++;

	if(i<3)
		goto start;
}


void print(NODE * list){
	while(list->next!=NULL){
		printf("%c ", list->data);
		list = list->next;
	}
	printf("\n");
}


void insert_first(NODE ** head,char c){
  NODE * temp = (NODE*)malloc(sizeof(NODE));
  temp->data = c;

  temp->next= (*head); // 1st NODE
  (*head) = temp;
}

void insert_second(NODE ** head, char c){
	NODE * temp = (NODE *)malloc(sizeof(NODE));
	temp->data = c;
	
	temp->next->next = (*head)->next; // 2nd NODE
	(*head)->next=temp;
}

void insert_third(NODE ** head, char c){
	NODE * temp = (NODE *)malloc(sizeof(NODE));
	temp->data = c;
	
	temp->next = (*head)->next->next; // 3rd NODE
	(*head)->next->next = temp;
}

void insert_last(NODE ** head, char c){
	NODE * temp = (NODE *)malloc(sizeof(NODE));
	temp->data = c;
	
	temp->next = (*head)->next->next->next; // 4th NODE
	(*head)->next->next->next = temp;
}
