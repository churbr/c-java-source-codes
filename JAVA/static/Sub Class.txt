/*
When we want that a class member should be used independently of any object of that class, we have to precede that member's declaration with the keyword static. 
When a member is declared as static, it can be accessed without creating an object of the class and without referring to the object.That static is, a static member belongs to a class as a whole and not to any one instance/object of that class.
The static member is shared by all objects of that class.
*/


public class SecondClass{
	private String fname;
	private String lname;
	private static int members; // what static means is it will be shared by all the variables/objects IN THE CLASS.


	public SecondClass(String fn, String ln){
		fname = fn;
		lname = ln;
		members++;
		
		System.out.printf("Members in the club %s %s\nMember: %d\n", fname, lname, members);
	}
	
	public String getFirst(){
		return fname;
	}
	
	public String getLast(){
		return lname;
	}
	
	public int getNumberOfMembers(){
		return members;
	}
}