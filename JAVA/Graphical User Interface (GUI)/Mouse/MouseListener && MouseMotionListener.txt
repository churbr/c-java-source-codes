import java.awt.*; // layout and etc
import java.awt.event.*; // To use the mouse listener
import javax.swing.*; // for GUI

public class Chocolates extends JFrame{
	
	private JPanel mouseArea; // Big Screen
	private JLabel status; // to see labels of what's happening
	
	public Chocolates(){
		super("MouseListener!");
			
		mouseArea = new JPanel(); 
		mouseArea.setBackground(Color.ORANGE); // set the default color
		add(mouseArea, BorderLayout.CENTER); // we set the layout of the mouse which is the panel to be in the center part of the GUI
		
		status = new JLabel("Nothing is happening!");
		add(status, BorderLayout.SOUTH); // put the status bar or the info to the bottom
		
		HandlerClass handler = new HandlerClass();
		mouseArea.addMouseListener(handler); // add them functions so we can use it later
		mouseArea.addMouseMotionListener(handler);
		
	}
	
	private class HandlerClass implements MouseListener, MouseMotionListener{
		// These are the 5 methods of MouseListener
		public void mouseClicked(MouseEvent clicked){
			mouseArea.setBackground(Color.blue);
			status.setText(String.format("You've clicked coordinates %dx%d", clicked.getX(), clicked.getY()));
		}
		
		public void mousePressed(MouseEvent pressed){
			status.setText("You've pressed the mouse!");
		}
		
		public void mouseReleased(MouseEvent released){
			status.setText("You have released the mouse!");
		}
		
		public void mouseEntered(MouseEvent entered){
			if(entered.getX()<200 && entered.getY()<200){
				JOptionPane.showMessageDialog(null, "Coordinates X and Y is less than 200.");
			}
			else if(entered.getX()>400 && entered.getY()>400){
				JOptionPane.showMessageDialog(null, "Coordinates X and Y is greater than 400.");
			}
			
			
			status.setText("You have entered the screeen or the panel.");
			mouseArea.setBackground(Color.DARK_GRAY);
		}
		
		public void mouseExited(MouseEvent exited){
			mouseArea.setBackground(Color.BLACK);
			status.setText("You have exited the screen!");
			mouseArea.setBackground(Color.GREEN);
		}
		
		// These methods are from the MouseMotionListener
		
		public void mouseDragged(MouseEvent dragging){
			mouseArea.setBackground(Color.cyan);
			status.setText("You're dragging somewhere in the screen!");
		}
		
		public void mouseMoved(MouseEvent moving){
			status.setText("You're moving the mouse.");
		}
		
	}
	
	public static void main(String sweets[]){
		Chocolates chu = new Chocolates();
		chu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		chu.setSize(1020,580);
		chu.setVisible(true);
	}
	
}