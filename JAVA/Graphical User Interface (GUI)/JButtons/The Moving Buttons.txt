import javax.swing.*;
import java.awt.event.*;
import java.awt.*;


public class Hersheys extends JFrame{
	
	private JButton leftButton;
	private JButton rightButton;
	private JButton centerButton;
	private FlowLayout layout; // we'll use customize layouting
	
	public Hersheys(){
		super("The Moving Buttons!");
		layout = new FlowLayout(); // we set a default layout for now
		
		
		leftButton = new JButton("Left"); // left button
		add(leftButton); // add it to the screen
		setLayout(layout); //we set the layout of left JButton to default
		leftButton.addActionListener(// brain added
				new ActionListener(){
					public void actionPerformed(ActionEvent e){
						/*
							Here, if we click the leftButton it will call the FlowLayout button named layout,
							which is declared as a new FlowLayout or simply default.
							so we set it from default to new Layout by setting the alignment of it.
							setAlignment = sets the Alignment of any stuff
							FlowLayout is a class that has many type of customized layouts.
							So we call FlowLayout to please let us use your left custom layout.
							so the layout is set to left
						*/
						
						layout.setAlignment(FlowLayout.LEFT);
						/*
							Since layout is already has the value of LEFT then se set left to all of the buttons,
							and how can we move all the buttons together by just clicking left?
							Well, that's the use of getContentPane() which means the sides of the windows.
							its pretty much saying the LEFT.layoutContainer(getContentPane())
							this simply says that hey left, I want you to be the layout of all the buttons the the pane.
							we set left to all of the buttons to be in the left side by calling layoutContainer,
							layoutContainer() is the container of the buttons. Since he is a container, when he will be move,
							others will be move also as layoutContainer moves and that getContentPane is layoutContainer must touch first content pane,
							so he can move as long as he wanna.						*/
						layout.layoutContainer(getContentPane());
					}
				}
			);
		
		centerButton = new JButton("center");
		setLayout(layout);
		add(centerButton);
		centerButton.addActionListener(
				new ActionListener(){
					public void actionPerformed(ActionEvent e){
						layout.setAlignment(FlowLayout.CENTER);
						layout.layoutContainer(getContentPane());
					}
				}
			);
		
		rightButton = new JButton("right");
		setLayout(layout);
		add(rightButton);
		rightButton.addActionListener(
				new ActionListener(){
					public void actionPerformed(ActionEvent e){
						layout.setAlignment(FlowLayout.RIGHT);
						layout.layoutContainer(getContentPane());
					}
				}
			);
	}
	
	public static void main(String args[]){
		Hersheys chu = new Hersheys();
		chu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		chu.setVisible(true);
		chu.setSize(400,80);
		chu.setResizable(false);
		
	}
}